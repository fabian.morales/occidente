<?php

class CiudadConvenio extends myEloquent {    
    protected $table = 'my_con_ciudad';
    
    function convenios(){
        return $this->belongsToMany('Convenio', 'my_con_ciudadconv', 'id_ciudad', 'id_convenio');
    } 
}
