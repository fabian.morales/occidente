<?php
    
use \Illuminate\Database\Capsule\Manager as Capsule;

class adminConvenioController extends myAdminController{
    public function __construct() {
        $doc = myApp::getDocumento();
        $doc->addScript(JUri::root()."media/jui/js/jquery.min.js");
        $doc->addScript(JUri::root()."media/editors/tinymce/tinymce.min.js");
        $doc->addEstilo(JUri::root()."media/jui/css/bootstrap.css");
        $doc->addEstilo(JUri::root()."myCore/css/foundation/css/foundation-grid.css");
        //bootstrap.min
    }
    
    public function index(){
        JToolbarHelper::title('Gestión de convenios');
        $convenios = Convenio::paginate(20);
        return myView::render("admin.convenio.index", ["convenios" => $convenios]);
    }        
    
    public function crearConvenio(){
        return $this->formConvenio(new Convenio());
    }
    
    public function editarConvenio(){
        $idCon = myApp::getRequest()->getVar("id");
        $convenio = Convenio::find($idCon);

        if (!sizeof($convenio)){
            myApp::redirect("index.php?option=com_my_component&controller=adminConvenio", "Convenio no encontrado");    
        }        

        return $this->formConvenio($convenio);
    }
    
    public function formConvenio($convenio){
        JToolbarHelper::title('Gestión de convenios');        
        
        $categorias = CategoriaConv::all();
        $ciudades = CiudadConvenio::with(array('convenios' => function($query) use ($convenio) {
            $query->where('id_convenio', $convenio->id);
        }))->orderBy("nombre")->get();

        return myView::render("admin.convenio.form", ["convenio" => $convenio, "categorias" => $categorias, "ciudades" => $ciudades]);
    }    
    
    public function guardarConvenio(){
        $request = myApp::getRequest();
        $idConv = myApp::getRequest()->getVar("id");
        $convenio = Convenio::find($idConv);
        
        if (!sizeof($convenio)){
            $convenio = new Convenio();
        }
        
        $convenio->fill($request->all());
        $convenio->servicios = $request->getVar("servicios", "", "RAW");
        $convenio->descuento = $request->getVar("descuento", "", "RAW");
        $convenio->condiciones = $request->getVar("condiciones", "", "RAW");
        
        
        $f = $_FILES["archivo"];
        if (is_array($f) && is_uploaded_file($f["tmp_name"]) && !$f['error']){		    
            $info = pathinfo($f['name']);
            $convenio->extension = strtolower($info["extension"]);
        }
        
        if ($convenio->save()){
            $ciudades = $request->getVar("ciudades", [], "ARRAY");            
            
            $convenio->ciudades()->detach();
            $convenio->ciudades()->attach($ciudades);
            
            if (is_array($f) && is_uploaded_file($f["tmp_name"]) && !$f['error']){
                $dirs = array(myApp::pathImg(), "convenios");

                $dir = "";
                foreach ($dirs as $d){
                    $dir .=$d.DS;
                    if(!is_dir($dir)){
                        @mkdir($dir);
                    }
                }
                
                $nombreArchivo = $convenio->id.".".$convenio->extension;
                move_uploaded_file($f['tmp_name'], $dir.DS.$nombreArchivo);
            }

            myApp::redirect("index.php?option=com_my_component&controller=adminConvenio", "Convenio guardado");
        }
        else{
            myApp::redirect("index.php?option=com_my_component&controller=adminConvenio", "No se pudo guardar el convenio");
        }
    }
    
    public function borrarConvenio(){
        $idConv = myApp::getRequest()->getVar("id");
        $convenio = Convenio::find($idConv);
        
        if (!sizeof($convenio)){
            myApp::redirect("index.php?option=com_my_component&controller=adminConvenio", "Convenio no encontrado");
        }
        
        $nombreArchivo = myApp::pathImg().DS."convenios".DS.$convenio->id.".".$convenio->extension;
        if ($convenio->delete()){
            @unlink($nombreArchivo);
            myApp::redirect("index.php?option=com_my_component&controller=adminConvenio", "Convenio borrado");
        }
        else{
            myApp::redirect("index.php?option=com_my_component&controller=adminConvenio", "No se pudo borrar el convenio");
        }
    }
}
