<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
@ini_set('display_errors', '1');
// Getting params from template
$params = JFactory::getApplication()->getTemplate(true)->params;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->getCfg('sitename');

if($task == "edit" || $layout == "form" )
{
	$fullWidth = 1;
}
else
{
	$fullWidth = 0;
}

// Add Stylesheets
JHtml::_('jquery.framework', true, true);
//$doc->addScript(JURI::root(true).'/media/jui/js/jquery.min.js');
$doc->addStyleSheet('templates/'.$this->template.'/foundation/css/foundation.min.css');
$doc->addStyleSheet(JURI::root(true).'/myCore/css/foundation/fonts/foundation-icons.css');
$doc->addScript('templates/'.$this->template.'/foundation/js/foundation.min.js');
$doc->addScript('templates/'.$this->template.'/foundation/js/vendor/modernizr.js');
$doc->addScript('templates/'.$this->template.'/js/lightSlider/jquery.lightSlider.min.js');
$doc->addScript('myCore/js/featherlight/featherlight.js');
$doc->addScript('templates/'.$this->template.'/js/nicescroll/jquery.nicescroll.min.js');
$doc->addStyleSheet('myCore/js/featherlight/featherlight.css');
$doc->addStyleSheet('templates/'.$this->template.'/js/lightSlider/lightSlider.css');
$doc->addStyleSheet('templates/'.$this->template.'/css/style.css');
$doc->addScript('templates/'.$this->template.'/js/scripts.js');
// Add current user information
$user = JFactory::getUser();
?>
<?php if(!$params->get('html5', 0)): ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php else: ?>
	<?php echo '<!DOCTYPE html>'; ?>
<?php endif; ?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jdoc:include type="head" />

	<!--[if lt IE 9]>
		<script src="<?php echo $this->baseurl ?>/media/jui/js/html5.js"></script>
	<![endif]-->
</head>
<body>
    <div id="top-link"></div>
    <jdoc:include type="modules" name="top-menu" style="xhtml" />
    <jdoc:include type="modules" name="banner" style="xhtml" />
    <jdoc:include type="modules" name="main-menu" style="xhtml" />
    <?php        
        $menu = $app->getMenu();
        if ($menu->getActive() != $menu->getDefault()) : ?>        
        <div class="component">
            <jdoc:include type="component" />
        </div>        
    <?php endif; ?>    
</body>
</html>
