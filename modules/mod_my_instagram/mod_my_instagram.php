<?php
    /*
	Héctor Fabián Morales Ramírez
	Tecnólogo en Ingeniería de Sistemas
	Octubre 2013
*/

require_once(JPATH_ROOT."/myCore/autoload.php");

$usuario = $params->get('usuario');
$limite = (int)$params->get('limite');
$c = myApp::getController("instagram");
echo $c->index($usuario, $limite);